#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void cambiar(int *a, int *b);

int main()
{
    //Declaración y asignación de las variables
    float n1, n2, *p1, *p2;
    n1 = 4.0;
    p1 = &n1;
    p2 = p1;
    n2 = *p2;
    n1 = *p1 + *p2;
    //¿Cuanto vale n1 y n2?
    printf("n1 vale: '%f' y\nn2 vale: '%f'\n ", n1, n2);
    return 0;
}
//------------------------------------------------------
//Output:
//n1 vale: '8.000000' y
//n2 vale: '4.000000'