#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main()
{
    //Declaración y asignación de las variables
    int i, a = 0;
    //Recorrer el loop for
    for (i = 0; i < 5; i++)
    {
        imprimir(&a);
    }
    return 0;
}

void imprimir(int *a)
{
    *a = *a + 1;
    printf("%d\n ", *a);
}
//------------------------------------------------------
//Output:
//  1
//  2
//  3
//  4
//  5