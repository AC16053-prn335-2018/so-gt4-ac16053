#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main()
{
    int *pnumero, num1, num2;
    char *pchar, letra1;
    num1 = 2;
    num2 = 5;
    letra1 = 'a';
    pnumero = &num1;
    printf("El valor de pnumero es %d\n ", *pnumero);
    return 0;
}

//------------------------------------------------------
//Output:
//2