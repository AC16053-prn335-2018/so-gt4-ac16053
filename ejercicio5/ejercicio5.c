#include <stdio.h>
#include <stdlib.h>

//Función comparadora para ordenar los valores del putnero
int cmpfunc(const void *a, const void *b)
{
    return (*(int *)a - *(int *)b);
}

int main()
{
    //Cambiar la semilla de la secuencia de números aleatorios
    srand(time(NULL));
    //Declarar variables
    int *sizeNum = NULL, n, i;
    //Perdir tamaño
    printf("¿Cuantos números desea almacenar?\n");
    //Guardarlos en la var n
    scanf("%d", &n);
    /* reservar memoria para almacenar n enteros */
    sizeNum = (int *)malloc(n * sizeof(int));
    /* Verificamos que la asignación se haya realizado correctamente */
    if (sizeNum == NULL)
    {
        printf("Problemas al reservar memoria\n");
        //Si hay problemas, se sale del programa
        return 1;
    }
    for (i = 0; i < n; i++)
    {
        //Parse a entero y asignación de numeros al azar con la función rand
        sizeNum[i] = (int)(rand() % 101);
        //Se imprime la posición y el valor que almacena
        printf("sizeNum[%d] = %d \n", i + 1, sizeNum[i]);
    }
    //Se ordena el array con la funcion Quick Sort (qsort)
    qsort(sizeNum, n, sizeof(int), cmpfunc);
    printf("Luego de ordenar\n");
    for (i = 0; i < n; i++)
    {
        //Se imprime la posición y el valor que almacena
        printf("sizeNum[%d] = %d \n", i + 1, sizeNum[i]);
    }
    return 0;
}
