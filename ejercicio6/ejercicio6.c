#include <stdio.h>
#include <stdlib.h>

//Función comparadora para ordenar los valores del putnero
int cmpfunc(const void *a, const void *b)
{
    return (*(float *)a - *(float *)b);
}

int main()
{
    //Declarar variables
    int n, i;
    float *sizeNum = NULL, media = 0.0;
    //Perdir tamaño
    printf("¿Cuantos números desea almacenar?\n");
    //Guardarlos en la var n
    scanf("%d", &n);
    /* reservar memoria para almacenar n enteros */
    sizeNum = (float *)malloc(n * sizeof(float));
    /* Verificamos que la asignación se haya realizado correctamente */
    if (sizeNum == NULL)
    {
        printf("Problemas al reservar memoria\n");
        //Si hay problemas, se sale del programa
        return 1;
    }
    for (i = 0; i < n; i++)
    {
        printf("¿Qué número quiere almacenar en la posición %d?\n", i + 1);
        //Guardarlos en la dirección de sizeNum en la posicion i
        scanf("%f", &sizeNum[i]);
        //Se van almacenando los valores que se ingresan
        media = media + sizeNum[i];
    }
    //Se ordena el array con la funcion Quick Sort (qsort)
    qsort(sizeNum, n, sizeof(float), cmpfunc);
    //Se imprimen las salidas
    printf("La media es: %f\nEl número menor es: %f\nEl número mayor es: %f\n", media / n, sizeNum[0], sizeNum[n - 1]);
    return 0;
}
