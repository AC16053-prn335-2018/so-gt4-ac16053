#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int main()
{
   //Declarar variables
   int n, i;
   double *coeficientes = NULL, punto, total;
   //Perdir grado del polinomio
   printf("¿Cúal es el grado del polinomio?\n");
   //Guardarlos en la var n
   scanf("%d", &n);
   /* reservar memoria para almacenar n enteros */
   coeficientes = (double *)malloc(n * sizeof(double));
   /* Verificamos que la asignación se haya realizado correctamente */
   if (coeficientes == NULL)
   {
      printf("Problemas al reservar memoria\n");
      //Si hay problemas, se sale del programa
      return 1;
   }
   for (i = 0; i <= n; i++)
   {
      printf("¿Cúal es el coeficiente de x^%d?\n", i);
      //Guardarlos en la dirección de sizeNum en la posicion i
      scanf("%lf", &coeficientes[i]);
      printf("coeficientes[%d] = %f \n", i + 1, coeficientes[i]);
   }
   printf("¿Cúal es el punto (x) a evaluar?\n");
   //Guardarlos en la var n
   scanf("%lf", &punto);
   for (i = 0; i <= n; i++)
   {
      total += coeficientes[i] * pow(punto, i);
      //Se imprime la posición y el valor que almacena
      printf("Total cuando i=%d es igual a %f \n", i, total);
   }
   return 0;
}
