#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>

//Prototipos de los metodos
char *pedirTexto();
void contarVocales(char *, int[]);
void imprimir(int[]);

void main()
{
    //Declaración y asignación de variables
    char *texto = (char *)malloc(sizeof(char));
    int num[5];
    texto = pedirTexto();
    printf("%s", texto);
    printf("\n%d", strlen(texto));
    printf("\n");
    contarVocales(texto, num);
    imprimir(num);
}

//En este método pediremos el texto a evaluar
char *pedirTexto()
{
    char *texto;
    texto = (char *)malloc(sizeof(char));
    printf("Ingrese un texto o palabra\n");
    scanf("%s", texto);
    return texto;
}

//Se cuenta el número de veces que se ha repetido una vocal
void contarVocales(char *texto, int vector[])
{
    //Inicializamos el arreglo a 0
    for (int i = 0; i < 5; i++)
    {
        vector[i] = 0;
    }
    //Recorremos el arreglo de chars en busca de vocales
    for (int i = 0; i <= strlen(texto); i++)
    {
        //Si encontramos alguna vocal, se sustituye por la que corresponda
        switch (toupper(texto[i]))
        {
        //Si la vocal es 'A' entonces se suma 1 a las veces que se ha encontrado
        case 'A':
            vector[0] += 1;
            break;
        //Si la vocal es 'E' entonces se suma 1 a las veces que se ha encontrado
        case 'E':
            vector[1] += 1;
            break;
        //Si la vocal es 'I' entonces se suma 1 a las veces que se ha encontrado
        case 'I':
            vector[2] += 1;
            break;
        //Si la vocal es 'O' entonces se suma 1 a las veces que se ha encontrado
        case 'O':
            vector[3] += 1;
            break;
        //Si la vocal es 'U' entonces se suma 1 a las veces que se ha encontrado
        case 'U':
            vector[4] += 1;
            break;
        default:
            break;
        }
    }
}

//Se imprime las veces que ha coincidido las vocales
void imprimir(int vector[])
{
    printf("La vocal a se repite %d veces\n", vector[0]);
    printf("La vocal e se repite %d veces\n", vector[1]);
    printf("La vocal i se repite %d veces\n", vector[2]);
    printf("La vocal o se repite %d veces\n", vector[3]);
    printf("La vocal u se repite %d veces\n", vector[4]);
}
