#include <stdio.h>
#include <stdlib.h>
#include <string.h>

//Se crea una estructura que contenga las palabras, este tipo de dato es llamado Palabra
typedef struct
{
    char palabra[20];
} Palabra;

//Prototipo de los metodos
void llenar(Palabra arreglo[]);
void imprimir(Palabra arreglo[]);
void ordenar(Palabra arreglo[]);

int main()
{
    //Se crea un arreglo del tipo de datos palabras
    Palabra palabras[5];
    //Se pide en consola que se escriban 5 palabras
    printf("Ingrese 5 palabras\n");
    //Se invoca el método encargado de llenar las palabras
    llenar(palabras);
    printf("Palabras desordenadas\n");
    //Se invoca el método que imprime en consola las palabras desordenadas
    imprimir(palabras);
    printf("Palabras ordenadas\n");
    //Se invoca el método encargado de ordenar las palabras
    ordenar(palabras);
    imprimir(palabras);
    return 0;
}

//En esta función se llenaran las palabras
void llenar(Palabra arreglo[])
{
    int i;
    //Se hace un loop para que pida las veces necesarias que ingrese un número
    for (i = 0; i < 5; i++)
    {
        printf("Ingrese la palabra#%d\n", i + 1);
        //Se guardan los valores en la posición indicada
        scanf("%s", arreglo[i].palabra);
    }
}

//Imprime las palabras ingresadas por el usuario
void imprimir(Palabra arreglo[])
{
    //Se hace un loop para que imprima las 5 palabras
    for (int i = 0; i < 5; i++)
    {
        printf("[%s]\n", arreglo[i].palabra);
    }
}

//Ordena las palabras en orden alfabetico
void ordenar(Palabra arreglo[])
{
    Palabra almacenar;
    int i;
    for (i = 4; i > 0; i--)
    {
        int j;
        for (j = 0; j < i; j++)
        {
            //char [20]=arreglo
            if (strcmp(arreglo[i].palabra, arreglo[j].palabra) < 0)
            {
                almacenar = arreglo[i];
                arreglo[i] = arreglo[j];
                arreglo[j] = almacenar;
            }
        }
    }
}
